//localStorage['myKey'] = 'somestring';

var base64Img = null;
margins = {
  top: 70,
  bottom: 40,
  left: 30,
  width: 550
};

var doc = new jsPDF();
var iduserglobal = localStorage['iduser'] || '';
var userglobal = localStorage['user'] || '';
var passwordglobal = localStorage['pass'] || '';
var name = localStorage['name'] || '';

if(userglobal.length>0 && passwordglobal.length>0)
{
	getUser(userglobal,passwordglobal);
}



//Agregar PDF
var options = {
    pdfOpenParams: {
        view: 'FitV',
        pagemode: 'thumbs',
        search: 'lorem ipsum'
    }
};
PDFObject.embed("pdf/capacitacionauditivos.pdf", "#example1");



// Crear Test

var myQuestions = [
    {
        question: "¿Qué es Vensi?",
        answers: {
            a: 'Respuesta a',
            b: 'Respuesta b',
            c: 'Respuesta c',
            d: 'Respuesta d'
        },
        correctAnswer: 'b'
    },
    {
        question: "¿Qué es Vensi?",
        answers: {
           	a: 'Respuesta a',
            b: 'Respuesta b',
            c: 'Respuesta c',
        },
        correctAnswer: 'c'
    }
];

var quizContainer = document.getElementById('quiz');
var resultsContainer = document.getElementById('results');
var submitButton = document.getElementById('submit');
var name = document.getElementById('name');
var result = document.getElementById('result');

//generateQuiz(myQuestions, quizContainer, resultsContainer, submitButton);

function generateQuiz(questions, quizContainer, resultsContainer, submitButton){

    function showQuestions(questions, quizContainer){
        // we'll need a place to store the output and the answer choices
        var output = [];
        var answers;

        // for each question...
        for(var i=0; i<questions.length; i++){
            
            // first reset the list of answers
            answers = [];

            // for each available answer...
            for(letter in questions[i].answers){

                // ...add an html radio button
                answers.push(
                    '<label>'
                    	+ letter 
                    	+ ': '
                        + '<input type="radio" name="question'+i+'" value="'+letter+'">	'
                        + ' '
                        + questions[i].answers[letter]
                    + '</label>	'
                );
            }

            // add this question and its answers to the output
            output.push(
                '<div class="question">' + questions[i].question + '</div><br>'
                + '<div class="answers">' + answers.join('') + '</div><br>'
            );
        }

        // finally combine our output list into one string of html and put it on the page
        quizContainer.innerHTML = output.join('');
    }


    function showResults(questions, quizContainer, resultsContainer){
        
        // gather answer containers from our quiz
        var answerContainers = quizContainer.querySelectorAll('.answers');
        
        // keep track of user's answers
        var userAnswer = '';
        var numCorrect = 0;
        
        // for each question...
        for(var i=0; i<questions.length; i++){

            // find selected answer
            userAnswer = (answerContainers[i].querySelector('input[name=question'+i+']:checked')||{}).value;
            
            // if answer is correct
            if(userAnswer===questions[i].correctAnswer){
                // add to the number of correct answers
                numCorrect++;
                
                // color the answers green
                answerContainers[i].style.color = '#2D3B6C';
            }
            // if answer is wrong or blank
            else{
                // color the answers red
                answerContainers[i].style.color = 'red';
            }
        }

        // show number of correct answers out of total
        resultsContainer.innerHTML = numCorrect + ' de ' + questions.length;
        
        
        if(numCorrect > 1)
        {
        
       		editResultado(numCorrect)
       	 
        }
        else
    	{
    			confirm("Revisa tus respuestas!");
    	}

    }

    // show questions right away
    showQuestions(questions, quizContainer);
    
    // on submit, show results
   //  submitButton.onclick = function(){
//         showResults(questions, quizContainer, resultsContainer);
//     }
    
    document.querySelector('#thirdPage').addEventListener('click', function(e){
		e.preventDefault();
		showResults(questions, quizContainer, resultsContainer);
		
		
		
	});
}


// Manejo de Scroll

 	var myFullpage = new fullpage('#fullpage', {
        menu: '#menu',
        sectionsColor: ['#FFFFFF', '#FFFFFF', '#FFFFFF'],
        autoScrolling: true,
        keyboardScrolling: false,
        scrollHorizontally: true
    });
    
    myFullpage.setAllowScrolling(false);
    
    document.querySelector('#firstPage').addEventListener('click', function(e){
		e.preventDefault();
		var email = document.getElementById("email").value;
		var pass = document.getElementById("psw").value;
		getUser(email,pass);
		
	});
	document.querySelector('#secondPage').addEventListener('click', function(e){
		e.preventDefault();
		
		// var r = confirm("¿Estas seguro de comenzar el test?");
// 		if (r == true) 
// 		{
//   			editSection(3);	
// 		} 
		editSection(2);	
	});

	
function exitclick(){
	var r = confirm("¿Deseas salir del curso?");
		if (r == true) 
		{
  			localStorage.clear();
  			fullpage_api.moveTo(1);
		} 

}


// Llamadas a Base de Datos

function getUser(emaillocal,passwordlocal){

	

	var obj, dbParam, xmlhttp, myObj;
    obj = { "user":emaillocal, "password":passwordlocal };
    var url = "http://157.245.184.224/api/v0/users/obtenerUsuario?email="+emaillocal+"&pass="+passwordlocal;
	
	dbParam = JSON.stringify(obj);
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
  		if (this.readyState == 4 && this.status == 200) {
    		myObj = JSON.parse(this.responseText);

                localStorage['iduser'] = myObj["id"];
    			localStorage['user'] = emaillocal;
    			localStorage['pass'] = passwordlocal;
    			
    			var section = myObj["section"];
    			document.getElementById('name').innerHTML = myObj["name"];
    			// document.getElementById('result').innerHTML = myObj[0]["resultado"];
    			fullpage_api.moveTo(section,0);
    		
    		
          }
        else if (this.readyState == 4 && this.status == 204)
        {
            confirm("Usuario o contraseña incorrecta!");
        }
	};
	xmlhttp.open("GET", url, true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send();


}

function editSection(sectionlocal){

    var url = "http://157.245.184.224/api/v0/users/"+localStorage['iduser']+"/";

	var obj, dbParam, xmlhttp, myObj;
	obj = { "section":sectionlocal};
	
	dbParam = JSON.stringify(obj);
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
  		if (this.readyState == 4 && this.status == 200) {
    		myObj = this.responseText;
    		
    		fullpage_api.moveSectionDown();
    		
    		
  		}
	};
	xmlhttp.open("PATCH", url, true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("x=" + dbParam);


}

function editResultado(resultadolocal){

	

	var obj, dbParam, xmlhttp, myObj;
	obj = { "resultado":resultadolocal,"user":localStorage['user'], "password":localStorage['pass'] };
	
	dbParam = JSON.stringify(obj);
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
  		if (this.readyState == 4 && this.status == 200) {
    		myObj = this.responseText;
    		if(myObj === "1")
    		{
    			editSection(4);	
    		}
    		else
    		{
    			confirm("Revisa tus respuestas!");
    		}
    		
  		}
	};
	xmlhttp.open("POST", "editresult.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("x=" + dbParam);


}

function downloadclick()
{
	var container = document.getElementById("container");
	
	 html2canvas(container, {scale: 1, allowTaint: true, logging: true}).then(canvas => {
         document.body.appendChild(canvas);
         	var img = canvas.toDataURL()
    		$.post("sendimage.php", {data: img}, function (file)
    		{
				//window.location.href =  "sendimage.php?file="+ file
				 $.post("sendimage.php",{file: file},function (data)
				{
					alert(data);
				});
    		});
     });
    


}


// document.getElementById('demosMenu').addEventListener('change', function(e){
//     var dropdown = document.getElementById('demosMenu');
//     window.location.href = dropdown.options[dropdown.selectedIndex].getAttribute('id') + '.html';
//     
//    
//     
//     
//     
//     
// });